package ObjectRepositories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TCPLoginPage {

	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public TCPLoginPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "LoginUserName")
	WebElement username;
	@FindBy(id = "LoginPassword")
	WebElement password;
	@FindBy(css = ".btn-primary")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() throws InterruptedException {
		Thread.sleep(1500);
		return login;
	}

}
