package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TCPHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public TCPHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}
	public void ContractorStatus() throws InterruptedException
	{
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.dropdown-toggle"))).click();
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("li.dropdown:nth-child(1) > span:nth-child(1) > span:nth-child(2)")));
		String uName=driver.findElement(By.cssSelector("li.dropdown:nth-child(1) > span:nth-child(1) > span:nth-child(2)")).getText();
		System.out.println("User Name : "+uName);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("statusButton")));
		String status=driver.findElement(By.id("statusButton")).getText();
		System.out.println("Contractor Status : "+status);
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".fa-home"))).click();
	}

	public void Logout() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.dropdown-toggle"))).click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("ul.dropdown-menu:nth-child(2) > li:nth-child(4) > a:nth-child(1)"))).click();
		Thread.sleep(2500);
		driver.quit();
	}

}
