package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.TCPHomePage;
import ObjectRepositories.TCPLoginPage;

public class TCPNetlinkGroup {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.contractorsportal-antwerpen.total.com/#contractor");

	}

	@Test
	public void TCPNelink() throws InterruptedException {
		TCPLoginPage tcpl = new TCPLoginPage(driver);
		tcpl.WaitFunction();
		tcpl.Username().sendKeys("manu.uppal@netlink-group.com");
		tcpl.Password().sendKeys("Netlink114_");
		tcpl.Login().click();
		TCPHomePage tcph = new TCPHomePage(driver);
		tcph.ContractorStatus();
		tcph.Logout();
	}

}
