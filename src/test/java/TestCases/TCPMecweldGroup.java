package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.TCPHomePage;
import ObjectRepositories.TCPLoginPage;

public class TCPMecweldGroup {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.contractorsportal-antwerpen.total.com/#contractor");

	}

	@Test
	public void TCPMecweld() throws InterruptedException {
		TCPLoginPage tcpl1 = new TCPLoginPage(driver);
		tcpl1.WaitFunction();
		tcpl1.Username().sendKeys("mecweldgroup@gmail.com");
		tcpl1.Password().sendKeys("Netlink222_");
		tcpl1.Login().click();
		TCPHomePage tcph1 = new TCPHomePage(driver);
		tcph1.ContractorStatus();
		tcph1.Logout();
	}

}
